# README #

## Závislosti ##
* Python 3.4
* BeautifulSoup

## Spustenie ##
```
import shopify_parser

shopify_parser.save_stores_to_csv(shopify_parser.get_stores_urls_from_csv('stores.csv'), 'output.csv', 5)

```

## Poznámky ##
* Súbor shopify_parser je rozdelený na 2 triedy a samostatné funkcie. Očakáva sa, že sa budú používať triedy, ale je umožnené použiť aj samostatné funkcie (sú hodnotné aj sami o sebe).
* Výstup je to klasické CSV, možno by bolo lepšie uzatvoriť hodnoty do úvodzoviek
* Pri získaní produktov z ```/collections/all``` sa berú aj tie, ktoré sú v navigácii alebo inde na stránke.
* Pri zapisovaní do CSV má obchod pevnú štruktúru (url, email, fb, tw). Aj keď na stránke nie sú zverejnené email, FB a twitter, na riadku je vždy minimálne ```http://storedomain.com,,,```. Počet produktov, ktoré sa sťahujú, sa dá meniť pomocou parametra a preto zvyšok riadku v csv nemá pevnú štruktúru.
* Ak url obchodu nefunguje, zapíše sa predsa do csv ale bez ďalších dodatočných informácií.