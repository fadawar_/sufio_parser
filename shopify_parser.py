#!/usr/bin/env python3

"""Parse information from stores created in shopify.com"""

__author__      = "fadawar"

import re
import requests
import json
import csv
from bs4 import BeautifulSoup
import time


def get_stores_urls_from_csv(filename):
    with open(filename) as file:
        next(file)  # skip header
        return ['http://' + line.strip() for line in file]


def save_stores_to_csv(stores_urls, filename, products_limit):
    with open(filename, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        for store_url in stores_urls:
            localtime = time.asctime(time.localtime(time.time()))
            print("{} Parsing {}\n".format(localtime, store_url))

            store = ShopifyStoreParser(store_url)
            row = store.to_list()
            products_links = store.products_links(products_limit)
            for link in products_links:
                try:
                    p = ShopifyProductParser(store_url + link).to_list()
                    row += p
                except ValueError:
                    pass  # if there is problem with retrieving product info we skip this product
            writer.writerow(row)


def find_link(html, href_pattern):
    soup = BeautifulSoup(html)
    links = soup.find_all('a', {'href': href_pattern})
    if not links:
        return None
    else:
        last_link = links[-1]  # hopefully, last link will be correct
        return last_link['href']


def find_facebook_link(html):
    pattern = re.compile(r'^https?://(www\.)?facebook\.com/.+')
    return find_link(html, pattern)


def find_twitter_link(html):
    pattern = re.compile(r'^https?://(www\.)?twitter\.com/.+')
    return find_link(html, pattern)


def find_email_link(html):
    pattern = re.compile(r'^(mailto:)[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$')
    email = find_link(html, pattern)
    if email:
        return email[7:]  # remove 'mailto:' from email
    else:
        return None


def find_products_links(html, limit):
    pattern = re.compile(r'^(/collections/all)?/products/.+')
    soup = BeautifulSoup(html)
    a_tags = soup.find_all('a', {'href': pattern})
    links = [a_tag['href'] for a_tag in a_tags]
    unique_links = []
    for link in links:
        if link not in unique_links:
            unique_links.append(link)

    return unique_links[:limit]


class ShopifyStoreParser:
    def __init__(self, url):
        self.url = url
        self.social_links_pages = {'/': None,  # front page
                                   '/pages/about': None,
                                   '/pages/about-us': None,
                                   '/pages/contact': None,
                                   '/pages/contact-us': None}

    def parse_social_links(self, function):
        for page, cached_html in self.social_links_pages.items():
            if not cached_html:
                cached_html = self.download_html(page)
                if not cached_html:
                    cached_html = ''
                self.social_links_pages[page] = cached_html
            link = function(cached_html)
            if link:
                return link
        return None

    def download_html(self, page):
        try:
            r = requests.get(self.url + page)
            if r.status_code == requests.codes.ok:
                return r.text
        except requests.exceptions.ConnectionError:
            return None

    def facebook_link(self):
        return self.parse_social_links(find_facebook_link)

    def twitter_link(self):
        return self.parse_social_links(find_twitter_link)

    def email(self):
        return self.parse_social_links(find_email_link)

    def products_links(self, limit):
        try:
            html = requests.get(self.url + '/collections/all/').text
            return find_products_links(html, limit)
        except requests.exceptions.ConnectionError:
            return []

    def to_list(self):
        fb = self.facebook_link()
        fb = fb if fb else ''  # if fb or twitter link does not exist, empty string is returned
        tw = self.twitter_link()
        tw = tw if tw else ''
        email = self.email()
        email = email if email else ''

        return [self.url, email, fb, tw]


class ShopifyProductParser:
    def __init__(self, url):
        product_json = requests.get(url + '.json').text
        product = json.loads(product_json)['product']
        self.title = product['title']
        self.image_url = None
        if product['image']:
            self.image_url = product['image']['src']

    def to_list(self):
        image_url = self.image_url if self.image_url else ''
        return [self.title, image_url]
