import unittest
from shopify_parser import get_stores_urls_from_csv, find_facebook_link, find_twitter_link, ShopifyStoreParser, \
    ShopifyProductParser, save_stores_to_csv, find_products_links, find_email_link


class ParseCSVTest(unittest.TestCase):
    def setUp(self):
        self.file = 'stores.csv'

    def test_can_read_stores_urls(self):
        stores_urls = get_stores_urls_from_csv(self.file)

        self.assertEqual(len(stores_urls), 100)
        self.assertEqual(
            stores_urls[:5],
            ['http://meoclock.myshopify.com', 'http://merchandiseprint.myshopify.com',
             'http://mercury-wheels.myshopify.com', 'http://merkledstore.myshopify.com',
             'http://merle-velma.myshopify.com']
        )


class FindingSocialLinksTest(unittest.TestCase):
    def setUp(self):
        self.html = '''
        <html>
        <body>
            <a href="http://twitter.com/MerleandVelma"></a>
            <a href="http://www.facebook.com/#!/merleandvelma"></a>
            <a href="mailto:info@mercurycycling.com"></a>
        </body>
        </html>'''

        self.html_https = '''
        <html>
        <body>
            <a href="https://twitter.com/MerleandVelma"></a>
            <a href="https://www.facebook.com/#!/merleandvelma"></a>
        </body>
        </html>'''

    def test_find_facebook_link(self):
        link = find_facebook_link(self.html)
        link_https = find_facebook_link(self.html_https)

        self.assertEqual(link, 'http://www.facebook.com/#!/merleandvelma')
        self.assertEqual(link_https, 'https://www.facebook.com/#!/merleandvelma')

    def test_find_twitter_link(self):
        link = find_twitter_link(self.html)
        link_https = find_twitter_link(self.html_https)

        self.assertEqual(link, 'http://twitter.com/MerleandVelma')
        self.assertEqual(link_https, 'https://twitter.com/MerleandVelma')

    def test_no_facebook_link(self):
        self.assertIsNone(find_facebook_link(''))

    def test_no_twitter_link(self):
        self.assertIsNone(find_twitter_link(''))

    def test_live_url(self):
        parser = ShopifyStoreParser('http://meoclock.com')
        self.assertEqual('https://www.facebook.com/meOclock', parser.facebook_link())
        self.assertEqual('http://twitter.com/meoclock', parser.twitter_link())

    def test_find_email(self):
        self.assertEqual(find_email_link(self.html), 'info@mercurycycling.com')
        self.assertIsNone(find_email_link(self.html_https))


class FindingProductsLinkTest(unittest.TestCase):
    def test_finding_links_online(self):
        parser = ShopifyStoreParser('http://meoclock.com')
        products = parser.products_links(5)
        self.assertEqual(len(products), 5)
        self.assertEqual(products,
                         ['/collections/all/products/amor',
                          '/collections/all/products/amor-1',
                          '/collections/all/products/amor-3',
                          '/collections/all/products/breast-cancer-time-pieces',
                          '/collections/all/products/black-breast-cancer-time-pieces'])

    def test_different_product_url_types(self):
        parser1 = ShopifyStoreParser('http://meoclock.com')  # /collections/all/products/*
        parser2 = ShopifyStoreParser('http://mercurycycling.com')  # /products/*
        links1 = parser1.products_links(1)
        links2 = parser2.products_links(1)

        self.assertEqual(links1[0], '/collections/all/products/amor')
        self.assertEqual(links2[0], '/products/alloyclincher')

    def test_no_products_urls(self):
        parser = ShopifyStoreParser('http://www.miik.ca')
        products = parser.products_links(5)
        self.assertEqual(len(products), 0)

    def test_fake_product_link(self):
        html = '<a href="/blabla/products/bla">bla</a>'
        self.assertEqual([], find_products_links(html, 5))


class ParsingProductJSONTest(unittest.TestCase):
    def test_finding_products_info(self):
        product = ShopifyProductParser('http://mercurycycling.com/products/brakepads')
        self.assertEqual(product.title, 'Carbon Specific Brake Pads')
        self.assertEqual(product.image_url,
                         'https://cdn.shopify.com/s/files/1/0266/7165/products/Mercury_Carbon_Brake_Pads-1.jpg?v=1385487670')

    def test_broken_url(self):
        self.assertRaises(ValueError, ShopifyProductParser, '__non-existing-url__')

    def test_404(self):
        self.assertRaises(ValueError, ShopifyProductParser, 'http://meoclock.com/products/_non_existing_product')

    def test_no_image(self):
        product = ShopifyProductParser('http://millcitybread.com/products/6-pack-artisan-bread-mix-assortment')
        self.assertEqual(product.title, '6-pack, Artisan Bread Mix assortment')
        self.assertIsNone(product.image_url)
        self.assertEqual(product.to_list(), ['6-pack, Artisan Bread Mix assortment', ''])


class SaveCSVTest(unittest.TestCase):
    def test_prepare_for_csv(self):
        url = 'http://meoclock.com'
        store = ShopifyStoreParser(url)
        self.assertEqual(store.to_list(), ['http://meoclock.com',
                                           '',
                                           'https://www.facebook.com/meOclock',
                                           'http://twitter.com/meoclock',
                                           ])
        products_links = store.products_links(5)
        products = [ShopifyProductParser(url + link).to_list() for link in products_links]
        self.assertEqual(products, [
            ['Amor', 'https://cdn.shopify.com/s/files/1/0240/6843/products/9.jpg?v=1372639676'],
            ['Amor', 'https://cdn.shopify.com/s/files/1/0240/6843/products/10.jpg?v=1372639698'],
            ['Amor',
             'https://cdn.shopify.com/s/files/1/0240/6843/products/Better_revised_heart_watch.jpg?v=1400255649'],
            ['Amor',
             'https://cdn.shopify.com/s/files/1/0240/6843/products/Closed_Heart_Pink_Cartoon_Image.jpg?v=1423429538'],
            ['Amor',
             'https://cdn.shopify.com/s/files/1/0240/6843/products/Black_Cartoon_Closed_Heart.jpeg?v=1423429331']
        ])

    def test_csv_output(self):
        url = 'http://meoclock.com'
        save_stores_to_csv([url], 'output.csv', 5)
        with open('output.csv') as f:
            line = next(f)
            self.assertEqual(line,
                             "http://meoclock.com,,https://www.facebook.com/meOclock,http://twitter.com/meoclock,"
                             "Amor,https://cdn.shopify.com/s/files/1/0240/6843/products/9.jpg?v=1372639676,"
                             "Amor,https://cdn.shopify.com/s/files/1/0240/6843/products/10.jpg?v=1372639698,"
                             "Amor,https://cdn.shopify.com/s/files/1/0240/6843/products/Better_revised_heart_watch.jpg?v=1400255649,"
                             "Amor,https://cdn.shopify.com/s/files/1/0240/6843/products/Closed_Heart_Pink_Cartoon_Image.jpg?v=1423429538,"
                             "Amor,https://cdn.shopify.com/s/files/1/0240/6843/products/Black_Cartoon_Closed_Heart.jpeg?v=1423429331"
                             "\n")

    def test_broken_url(self):
        url = 'http://meoclock-non-existing-url-123456.com'
        save_stores_to_csv([url], 'output.csv', 5)
        with open('output.csv') as f:
            line = next(f)
            self.assertEqual(line, "http://meoclock-non-existing-url-123456.com,,,\n")
